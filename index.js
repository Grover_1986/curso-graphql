import { ApolloServer, gql } from "apollo-server"
import {v4 as uuid} from 'uuid'

/*** ORIGEN DE DATOS ***/
const persons = [
    {
        name: 'Midu',
        phone: '034-1234567',
        street: 'Calle Frontend',
        city: 'Barcelona',
        id: '3d594650-3436-11e9-bc57-8b80ba54c431'
    },
    {
        name: 'Youseff',
        phone: '044-123456',
        street: 'Avenida Fullstack',
        city: 'Mataro',
        id: '3d594650-3436-11e9-bc57-8b80ba54c431'
    },
    {
        name: 'Itzi',
        street: 'Pasaje Testing',
        city: 'Ibiza',
        id: '3d594650-3436-11e9-bc57-8b80ba54c431'
    }
]

/**** DESCRIBIR LOS DATOS (definición de Tipos) ***/
// para esto es importante utilizar graphql con el método gql q directamente ejecuta un string, y en este caso ejecuta un template string
const typeDefinitions  = gql`
    type Address {
        street: String!
        city: String!
    }

    type Person {
        name: String!
        phone: String
        address: Address!
        id: ID! 
    }

    type Query {
        personCount: Int! 
        allPersons: [Person]! 
        findPerson(name: String!): Person 
    } 

    # ahora veremos como se cambian los datos, para eso usaremos las Mutaciones (Mutations)
    # ya hemos visto las QUERIES ahora veremos los MUTATIONS
    # ahora vamos añadir personas pero antes vamos a definir nuestros, ya q eso siempre se debe hacer en graphql
    type Mutation {
        addPerson(  # no le añadimos un id xq eso lo haremos en el Servidor
            name: String!
            phone: String
            street: String!
            city: String!
        ): Person  # la Mutacion devuelve en este caso a las persona q hemos añadido
    }
`

/**** RESOLVERS (solucionadores) ****/
// aquí veremos como se pueden resolver esos datos, x ejm en personCount de donde sale ese nro?
const resolvers = {
    Query: {
        personCount: () => persons.length,
        allPersons: () => persons,
        findPerson: (root, args) => { 
            const {name} = args
            return persons.find(person => person.name === name)
        }   
    },

    Person: {
        address: (root) => {
            return {
                street: root.street,
                city: root.city
            }
        }
    }
}

/**
 * Ahora q ya tenemos las definiciones de datos como tmb los resolvers vamos
 * a crear nuestro SERVIDOR
 */
const server = new ApolloServer({
    typeDefs: typeDefinitions,
    resolvers,
})

// iniciamos el Servidor
server.listen().then(({url}) => {
    console.log(`Server ready at ${url}`) 
})  